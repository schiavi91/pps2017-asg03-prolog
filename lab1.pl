% Ex1.1
% search(Elem,List)
search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

% Ex1.2
% search2(Elem,List)
% looks for two consecutive occurrences of Elem
search2(X,[X,X|_]).
search2(X,[_|Xs]):-search2(X,Xs).

% Ex1.3
% search_two(Elem,List)
% looks for two occurrences of Elem with an element in between!
search_two(X,[X,_,X|_]).
search_two(X,[_|Xs]):-search_two(X,Xs).
% It is fully relational

% Ex1.4
% search_anytwo(Elem,List)
% looks for any Elem that occurs two times
search_anytwo(X,[X|Xs]):-search(X,Xs).
search_anytwo(X,[_|Xs]):-search_anytwo(X,Xs).

% Ex2.1
% size(List,Size)
% Size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.
% It is fully relational but if we use a variable as the first argument, it only works the first time, then it enters an infinite loop

% Ex2.2
% size(List,Size)
% Size will contain the number of elements in List,
% written using notation zero, s(zero), s(s(zero))..
size_rel([],zero).
size_rel([_|T],s(M)) :- size_rel(T,M).
% It is fully relational

% Ex 2.3
% sum(List,Sum)
sum([],0).
sum([X|Xs],S):-sum(Xs,P), S is P+X.

% Ex2.4
% average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A):-average(List,0,0,A).
average([],C,S,A):-A is S/C.
average([X|Xs],C,S,A):-C2 is C+1,S2 is S+X,average(Xs,C2,S2,A).

% Another solution using sum and size defined before
average_two(L,A):-sum(L,Sum),size(L,Size),A is Sum/Size.

% Ex2.5
% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
% first solution: 3 arguments
max([X|Xs],M) :- max(Xs,X,M).
max([],M,M).
max([X|Xs],Mt,M) :- Mt>X, max(Xs,Mt,M),!.
max([X|Xs],Mt,M) :- Mt=<X, max(Xs,X,M).
% (cond -> then;else) esempio: max([X|Xs],Mt,M) :- (Mt>X -> max(Xs,Mt,M); max(Xs,X,M)).

% Previous solution with conditions
max_two([X|Xs],M):-max_two(Xs,X,M).
max_two([],M,M).
max_two([X|Xs],Mt,M):-(Mt>X -> max_two(Xs,Mt,M); max_two(Xs,X,M)).

% max_first(List,Max)
% Max with 2 arguments
% Suppose the list has at least one element
max_first([X],X).
max_first([X1,X2|Xs],M) :- (X1>X2 -> max_first([X1|Xs],M); max_first([X2|Xs],M)).

% max_second(List,Max)
% Another solution of max with 2 arguments and using the conditions
max_second([],0).
max_second([X|Xs],M):-max_second(Xs,Pm), (Pm<X -> M=X; M=Pm).

% max_third(List,Max)
% Another solution of max with 2 arguments and using the cut
max_third([],0).
max_third([X|Xs],X):-max_third(Xs,Pm), Pm<X,!.
max_third([X|Xs],Pm):-max_third(Xs,Pm).

% Ex3.1
% same(List1,List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

% Ex3.2
% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1
all_bigger([],[]).
all_bigger([H1|T1],[H2|T2]):-H1>H2,all_bigger(T1,T2).

% Ex3.3
% sublist(List1,List2)
% List1 should be a subset of List2
sublist([],_).
sublist([X|Xs],L):-search(X,L),sublist(Xs,L).

% Ex4.1
% seq(N,List)
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).
% It is not fully relational

% Ex4.2
% seqR(N,List)
seqR(0,[0]).
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

% Ex4.3
% seqR2(N,List)
seqR2(N,List):-seqR2(N,0,List).
seqR2(0,A,[A]).
seqR2(N,A,[A|T]):- N > 0, N2 is N-1, A2 is A+1, seqR2(N2,A2,T).

% last(List1,N,List2)
last([],N,[N]).
last([H|T1],N,[H|T2]):-last(T1,N,T2).

% first(List1,N,List2).
first(List1,K,[K|Ys]):- same(List1,Ys).

% seqR3(N,List)
% Another solution of seqR2 using last predicate
seqR3(0,[0]).
seqR3(N,L):- N > 0, N2 is N-1, seqR3(N2,L2), last(L2,N,L).

% Other exercises
% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
inv(List1,List2) :- inv(List1,List2,[]).
inv([], L, L).
inv([H|T],L,A):- inv(T,L,[H|A]).

% double(List,List)
% suggestion: remember predicate append/3
double(L1,L2):-append(L1,L1,L2).

% times(List,N,List)
% Recursive tail solution
times(_,0,[]).
times(L1,N,L2):-N>0,N2 is N-1,append(L1,Lt,L2),times(L1,N2,Lt).

% times_two(List,N,List)
% Non-recursive tail solution
times_two(_,0,[]).
times_two(L1,N,L2):-N>0,N2 is N-1,times_two(L1,N2,Lt),append(L1,Lt,L2).

% proj(List,List)
proj([],[]).
proj([[H|_]|T1],[H|T2]):-proj(T1,T2).
