% players can only be x or o
player(x).
player(o).

% changePlayer(?Previous,?Next)
changePlayer(x,o).
changePlayer(o,x).

% validMove(@Table,@Player,?NewTable)
% Given a Table, a player and the next table, check if the move is valid.
% Given a Table and a player, generate a valid move.
% example: validMove([[x,n,n],[n,o,n],[n,x,n]],o,X).
validMove([[H11,H2,H3]|T],P,[[H12,H2,H3]|T]):-H11=n,H12=P.
validMove([[H1,H21,H3]|T],P,[[H1,H22,H3]|T]):-H21=n,H22=P.
validMove([[H1,H2,H31]|T],P,[[H1,H2,H32]|T]):-H31=n,H32=P.
validMove([[H1,H2,H3]|T1],P,[[H1,H2,H3]|T2]):-validMove(T1,P,T2).

% checkWin(@Table,@Player,-Result)
% Given a Table and a Player, check if the player has won
% Result can be win(x), win(o) or nothing
checkWin([[P,P,P],[_,_,_],[_,_,_]],P,R):-!,R=win(P).
checkWin([[_,_,_],[P,P,P],[_,_,_]],P,R):-!,R=win(P).
checkWin([[_,_,_],[_,_,_],[P,P,P]],P,R):-!,R=win(P).
checkWin([[P,_,_],[P,_,_],[P,_,_]],P,R):-!,R=win(P).
checkWin([[_,P,_],[_,P,_],[_,P,_]],P,R):-!,R=win(P).
checkWin([[_,_,P],[_,_,P],[_,_,P]],P,R):-!,R=win(P).
checkWin([[P,_,_],[_,P,_],[_,_,P]],P,R):-!,R=win(P).
checkWin([[_,_,P],[_,P,_],[P,_,_]],P,R):-!,R=win(P).
checkWin([Row,_,_],P,R):-member(n,Row),!,R=nothing.
checkWin([_,Row,_],P,R):-member(n,Row),!,R=nothing.
checkWin([_,_,Row],P,R):-member(n,Row),!,R=nothing.
checkWin(T,P,R):-R=even.

% next(@Table,@Player,-Result,-NewTable)
next(Table,Player,Result,NewTable):-validMove(Table,Player,NewTable),checkWin(NewTable,Player,Result).

% allNext(@Table,@Player,-Result)
% Given a Table and a Player, it generates a list containing all the pairs (NewTable, Result) of all the possible moves
allNext(Table,Player,Result):-player(Player),findall((N,R),next(Table,Player,R,N),Result).

% game(@Table,@Player,-Result,-TableList)
game(Table,Player,Result,TableList):-
	next(Table,Player,R,Nt),
	R=win(_),!,
	Result=R,
	append(Table,[Nt],TableList).
game(Table,Player,Result,TableList):-
	next(Table,Player,R,Nt),
	R=even,!,
	Result=R,
	append(Table,[Nt],TableList).
game(Table,Player,Result,TableList):-
	next(Table,Player,R,Nt),
	changePlayer(Player,P),
	game(Nt,P,Result,NewT),
	append([Table],NewT,TableList).
