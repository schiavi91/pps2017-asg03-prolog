% Ex1.1
% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% dropFirst(?Elem,?List,?OutList)
% Drops only the first occurrence (showing no alternative results)
dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

% dropLast(?Elem,?List,?OutList)
% Drops only the last occurrence (showing no alternative results)
dropLast(X,[H|Xs],[H|L]):-dropLast(X,Xs,L),!.
dropLast(X,[X|T],T).

% dropAll_one(?Elem,?List,?OutList)
% Drop all occurrences, returning a single list as result
dropAll_one(_,[],[]).
dropAll_one(X,[Y|Xs],L):-copy_term(X,Y),!,dropAll_one(X,Xs,L).
dropAll_one(X,[H|Xs],[H|L]):-dropAll_one(X,Xs,L).

% dropAll(?List,?Elem,?OutList)
% Drop all occurrences, returning a single list as result
dropAll(LI,E,LO):-findall(LT,(member(LT,LI),E\=LT),LO).

% Ex2.1
% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% Ex2.2
% fromCircList(+List,-Graph)
fromCircList([X|Xs],G):-fromCircList([X|Xs],G,X).
fromCircList([H],[e(H,F)],F).
fromCircList([H1,H2|T],[e(H1,H2)|L],F):- fromCircList([H2|T],L,F).

% Ex2.3
% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1
dropNode(G,N,O):-dropAll(G,e(N,_),G2),dropAll(G2,e(_,N),O).

% Ex2.4
% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined with member(?Elem,?List)
reaching(G,N,L):-findall(X,(member(e(N,X),G)),L).

% Ex2.5
% anypath_one(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
anypath_one([e(N1,N2)|_],N1,N2,[e(N1,N2)]).
anypath_one([e(N1,N3)|T],N1,N2,L):-anypath_one(T,N3,N2,L1),append([e(N1,N3)],L1,L).
anypath_one([_|T],N1,N2,L):-anypath_one(T,N1,N2,L).

% anypath(+Graph, +Node1, +Node2, -ListPath)
% Another solution of anypath_one using member
anypath(G,N1,N2,[e(N1,N2)]):-member(e(N1,N2),G).
anypath(G,N1,N3,L):-member(e(N1,N2),G),anypath(G,N2,N3,L1),append([e(N1,N2)],L1,L).

% Ex2.6
% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
allreaching(G,N,L):-findall(Lt,anypath(G,N,Lt,_),L).
